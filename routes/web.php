<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

Route::get('/users/{token}', function($token) {
    $user = App\User::where('api_token', $token)->first();
    return response(['user'=>$user]);
});



Route::post('/login','AuthController@login');   
Route::get('/logout','AuthController@logout');
Route::post('/register','AuthController@register');


Route::get('/boards','BoardController@index');
Route::post('/boards','BoardController@store');
Route::get('/boards/{board}','BoardController@show');
Route::put('/boards/{boards}','BoardController@update');
Route::delete('/boards/{boards}','BoardController@destroy');



Route::get('/boards/{board}/list','ListController@index');
Route::post('/boards/{board}/list','ListController@store');
Route::get('/boards/{board}/list/{list}','ListController@show');
Route::put('/boards/{board}/list/{list}','ListController@update');
Route::delete('/boards/{board}/list/{list}','ListController@destroy');



Route::get('/boards/{board}/list/{list}/card','CardController@index');
Route::post('/boards/{board}/list/{list}/card','CardController@store');
Route::get('/card/{card}','CardController@show');

Route::put('/card/{card}','CardController@update');
Route::delete('/card/{card}','CardController@destroy');


/*
Route::put('/boards/{board}/list/{list}/card/{card}','CardController@update');
Route::delete('/boards/{board}/list/{list}/card/{card}','CardController@destroy');
*/